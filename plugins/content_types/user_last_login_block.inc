<?php
$plugin = array(
  'single' => TRUE,
  'title' => t('User Last Login Block'),
  'description' => t('Displays a block to show when the user last logged in.'),
  'category' => t('Widgets'),
  'edit form' => 'user_last_login_block_edit_form',
  'render callback' => 'user_last_login_block_render',
  'admin info' => 'user_last_login_block_admin_info',
  'defaults' => array(
    'message' => 'You last logged in at',
    'date_format' => 'g:i A - M j, Y',
  )
);

/**
 * 'admin info' callback for panel pane.
 */
function user_last_login_block_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    global $user;
    $date = format_date($user->login, 'custom', $conf['date_format'], $user->timezone, $user->language);

    $block = new stdClass;
    $block->title = $conf['override_title'] ? $conf['override_title_text'] : '';
    $block->content = array(
      '#markup' => $conf['message'] . '<br>' . $date,
      '#attached' => array(
        'js' => array(
          drupal_add_js(drupal_get_path('module', 'user_last_login_block') . '/js/animate.js'),
        ),
      ),
    );
    return $block;
  }
}

/**
 * 'Edit form' callback for the content type.
 */
function user_last_login_block_edit_form($form, &$form_state) {
  $form['message'] = array(
    '#type' => 'textfield',
    '#title' => t('Message'),
    '#size' => 60,
    '#description' => t('This message will appear followed by the time when the user last logged in.'),
    '#default_value' => $form_state['plugin']['defaults']['message'],
  );
  $form['date_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Date format'),
    '#size' => 60,
    '#description' => t('The date format used for the time when the user last logged in. See !date.', array('!date' => l('PHP date formats', 'http://php.net/manual/en/function.date.php', array('html' => TRUE, 'attributes' => array('external' => TRUE))))),
    '#default_value' => $form_state['plugin']['defaults']['date_format'],
  );
  return $form;
}

/**
 * The submit form stores the data in $conf.
 */
function user_last_login_block_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Run-time rendering of the body of the block (content type).
 */
function user_last_login_block_render($subtype, $conf, $panel_args, $context = NULL) {
  global $user;
  $date = format_date($user->login, 'custom', $conf['date_format'], $user->timezone, $user->language);

  $block = new stdClass();
  $block->title = $conf['override_title'] ? $conf['override_title_text'] : '';
  $block->content = array(
    '#markup' => $conf['message'] . '<br>' . $date,
    '#attached' => array(
      'js' => array(
        drupal_add_js(drupal_get_path('module', 'user_last_login_block') . '/js/animate.js'),
      ),
    ),
  );
  return $block;
}
