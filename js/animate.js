(function ($) {
  Drupal.behaviors.userLastLoginBlock = {
    attach: function (context) {
      $("#block-user-last-login-block-user-last-login-block").delay(10000).fadeOut(1000);
    }
  };
})(jQuery);
